# Mike Jang's Qualifications

Mike Jang's qualifications for the Beyond Identity Technical Writer position.

- Resume: mjangResume_beyondIdentity.docx
- Writing Samples: listOfWritingSamples.md
- Cover Letter: coverLetter_beyondIdentity.docx
- Speaking Experience: speakingExperience.md

