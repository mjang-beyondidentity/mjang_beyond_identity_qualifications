Mike Jang's conference speaking experience

- UI Text, Simplicity is Difficult (Linuxconf Australia 2021, STC 2023)
- When Linux on the Desktop is a Second Class Corporate Citizen (Open Source Summit, 2020, Fossy.us 2023)
- A Community That Meets Together Flourishes Together (Open Source Summit, 2019)
- How I Learned to Stop Worrying and Love the Command Line (Write the Docs, 2019).
- Minimum Viable Documentation for RESTful APIs (API Strategy Conference, 2018).
- Nurturing Global Meetups (Community Leadership Summit, 2018).
- Setting Up the Integrated ForgeRock Platform (ForgeRock Identity Live UnSummit, 2018).
- UI Text: Simplicity is Difficult (OSCON 2017).
- Help Deployers Run Your Webapps (O’Reilly Fluent Conference, 2016).
- Start Your Own Write the Docs Meetup Group (Write the Docs Conference, 2015).
- Ten Steps to a Better README (OSCON Ignite, 2015).
- When a Tech Writer Becomes a Systems Administrator (OSCON Ignite, 2014).
